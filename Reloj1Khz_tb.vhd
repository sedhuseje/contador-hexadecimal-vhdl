----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 11:34:04
-- Design Name: 
-- Module Name: Reloj1Khz_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Reloj1Khz_tb is
--  Port ( );
end Reloj1Khz_tb;

architecture Behavioral of Reloj1Khz_tb is

    Component Reloj1Khz is
    Port(      CLK100MHZ : in  STD_LOGIC;
              CLK1Khz : out  STD_LOGIC
         );
         end component;
     
     signal CLK100MHZ: std_logic:='0';
     signal CLK1Khz: STD_LOGIC := '0';
     
begin

inst_Reloj1KHz: Reloj1Khz port map(
        CLK100MHZ=>CLK100MHZ,
        CLK1KHz => CLK1KHz
        );
    
 CLK100MHZ <= not CLK100MHZ after 0.001ns;
end Behavioral;
