library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Divisor is
    Port ( CLK100Mhz : in  STD_LOGIC;
           
           CLK1hz   : out STD_LOGIC
);
end Divisor;

architecture Behavioral of Divisor is
signal CLK1HZ_i: STD_LOGIC := '0';
signal contador: integer range 0 to 49999999 := 0;


begin
process (CLK100Mhz)
begin
if (CLK100Mhz'event and CLK100Mhz = '1') then 
            if (contador = 49999999) then
                CLK1HZ_i <= NOT(CLK1HZ_i);
                contador <= 0;
            else
                contador <= contador+1;
            end if;
    end if;
  end process;
 CLK1hz <= CLK1HZ_i;
end Behavioral;
