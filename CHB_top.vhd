library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CHB_top is
    Port ( clk_100mhz, reset,startstop, Counting_way, Count_on, Count_off : in  STD_LOGIC;
           LED_countoff, LED_counton, LED_reloj : out STD_LOGIC;
           display_selection : out  STD_LOGIC_VECTOR (3 downto 0);
           display_number : out  STD_LOGIC_VECTOR (6 downto 0));
end CHB_top;

architecture Behavioral of CHB_top is
signal X1hz,X1khz: std_logic;
signal XDec: STD_LOGIC_VECTOR (3 downto 0);
signal XUni: STD_LOGIC_VECTOR (3 downto 0);
signal XBin: STD_LOGIC_VECTOR (3 downto 0);

component Divisor  is
    Port ( CLK100Mhz : in  STD_LOGIC;
           CLK1hz   : out STD_LOGIC);
end component;

component Contador is
    Port ( clock : in  STD_LOGIC;
           Clear_n, Enable, Counting_way, count_on, count_off, clk_100mhz: in  STD_LOGIC;
           LED_countoff, LED_counton, LED_reloj: out STD_LOGIC;
           Dec,Uni : inout  STD_LOGIC_VECTOR (3 DOWNTO 0));
end component;

component Mux21 is 
    Port ( Decenas  : in  STD_LOGIC_VECTOR (3 downto 0);
           Unidades : in  STD_LOGIC_VECTOR (3 downto 0);
           Selector : in  STD_LOGIC;
           Salidas  : out  STD_LOGIC_VECTOR (3 downto 0));
end component;

component Dec7segm is 
port(  BINARIO: in  STD_LOGIC_VECTOR (3 downto 0);
       led: out STD_LOGIC_VECTOR(6 downto 0) );  
end component;

component RELOJ1KHZ is
    Port ( CLK100MHZ : in  STD_LOGIC;
           CLK1KHZ : out  STD_LOGIC);
end component;

component Display_selector is 
port( input: in std_logic;
      anodo: out std_logic_vector(3 downto 0));
end component;

begin
paso1: Divisor         
PORT MAP (
--clk_100mhz, X1hz
CLK100Mhz => clk_100mhz,
CLK1hz => X1hz
);

paso2: Contador        
PORT MAP (
--X1hz,reset,startstop,XDec,XUni
clock => X1hz,
clk_100mhz=>clk_100mhz,
Clear_n => reset,
Count_off=>Count_off,
Count_on=>Count_on,
LED_countoff=>LED_countoff,
LED_reloj=>LED_reloj,
LED_counton=>LED_counton,
Counting_way=> counting_way,
Enable => startstop,
Dec => XDec,
Uni => XUni
);

paso3: Mux21           
PORT MAP (
--XDec,XUni,X1khz,XBin
Decenas => XDec,
Unidades => XUni,
Selector => X1khz,
Salidas => XBin
);

paso4: Dec7segm  
PORT MAP (
--XBin,display_number
BINARIO => XBin,
led => display_number
);

paso5: RELOJ1KHZ      
PORT MAP (
--clk_100mhz, X1khz
CLK100MHZ => clk_100mhz,
CLK1KHZ => X1khz
);

paso6: Display_selector 
PORT MAP (
--X1khz,display_selection
input => X1khz,
anodo => display_selection
);
end Behavioral;
