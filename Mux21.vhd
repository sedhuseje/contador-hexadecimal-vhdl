----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.12.2018 15:50:11
-- Design Name: 
-- Module Name: Mux2a1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Mux21 is
    Port ( Decenas  : in  STD_LOGIC_VECTOR (3 downto 0);
           Unidades : in  STD_LOGIC_VECTOR (3 downto 0);
           Selector : in  STD_LOGIC;
           Salidas  : out  STD_LOGIC_VECTOR (3 downto 0));
end Mux21;

architecture Behavioral of Mux21 is

begin

 Salidas <= Decenas WHEN Selector = '0' ELSE 
           Unidades; 

end Behavioral;
