----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.12.2018 17:07:24
-- Design Name: 
-- Module Name: Contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Contador is
    Port ( clock : in  STD_LOGIC;
           Clear, Enable, Counting_way, Count_on : in  STD_LOGIC;
           LED_Countoff : out STD_LOGIC;
           Dec,Uni : inout  STD_LOGIC_VECTOR (3 DOWNTO 0));
end Contador;

architecture Behavioral of Contador is
type state_type is (S0,S1);
signal state, next_state: state_type;
begin 
    change_st: process(Count_on)
        begin
           if Count_on = '0' then
            state<=S0;
          else if Count_on = '1' then
            state<=S1;
            end if;
            end if;
         end process;
  PROCESS(clock, Clear, Counting_way)
  
  begin
  case (state) is
   when S0=>
        LED_Countoff<='1';
   when S1=>
        LED_Countoff<='0';
   if clock'event and clock = '1' then
    if Clear = '1' then
      Dec <= "0000"; Uni <= "0000";
      
    elsif Enable = '1' then
            if Counting_way = '0' then
                if Uni = "1111" then 
                    Uni <= "0000";
                    if Dec = "0010" then
                        Dec <= "0000";
                    else
                        Dec <= Dec + '1';
                end if;
                else
                    Uni <= Uni +  '1';   
            end if;
         
            else if Counting_way = '1' then
                 if Uni = "0000" then 
                     Uni <= "1111";
                     if Dec = "0000" then
                         Dec <= "0010";
                     else
                         Dec <= Dec - '1';
                 end if;
                 else
                     Uni <= Uni -  '1';      
            end if;    
            end if;   
   end if;  
   end if;
   end if;
   end case;
 end PROCESS;  
 end  Behavioral;