library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Contador_tb is
--  Port ( );
end Contador_tb;

architecture Behavioral of Contador_tb is

    component contador is
    Port(
        clock: in std_logic;
        Clear: in std_logic;
        Enable: in std_logic;
        COunt_on: in std_logic;
        Counting_way: in std_logic;
        LED_countoff: out std_logic;
        Dec, Uni: inout std_logic_vector(3 DOWNTO 0)
     );
     
     end component;
    
signal clock: std_logic := '0';
signal Clear, Enable: std_logic;
signal Counting_way:std_logic:='0';
signal Count_on: std_logic:='1';
signal Dec, Uni: std_logic_vector(3 DOWNTO 0);
signal LED_Countoff: std_logic:='0';

begin

inst_contador: contador port map(
    clock=>clock,
    Clear=>Clear,
    Count_on=>Count_on,
    Counting_way=>Counting_way,
    Enable=>Enable,
    Dec=>Dec,
    Uni=>Uni,
    LED_Countoff=>LED_Countoff
 );
    
clock <= not clock after 100ns;
Counting_way<= not Counting_way after 4000ns;
Count_on<=not Count_on after 10000ns;
process
begin 

    Clear <= '1' ;
    wait for 425 ns;
    Clear<='0';
    wait for 50 ns;
    Enable <= '0';
    wait for 200 ns;
    Enable <= '1';
    wait;
end process;
end Behavioral;
