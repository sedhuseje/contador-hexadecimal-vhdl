----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.12.2018 17:07:24
-- Design Name: 
-- Module Name: Contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Contador is
    Port ( clock : in  STD_LOGIC;
           Clear_n, clk_100mhz, Enable, Counting_way, Count_on, Count_off : in  STD_LOGIC;
           LED_Countoff, LED_counton, LED_reloj: out STD_LOGIC;
           Dec,Uni : inout  STD_LOGIC_VECTOR (3 DOWNTO 0));
end Contador;

architecture Behavioral of Contador is
signal next_dec: STD_LOGIC_VECTOR (3 DOWNTO 0);
signal next_uni: STD_LOGIC_VECTOR (3 DOWNTO 0);
type state_type is (S0,S1);
signal state, next_state: state_type;
begin
    change_st: process(clock,Clear_n,Count_on, Count_off,clk_100mhz)
    begin
        if Clear_n = '0' then
            state <= S0;
            Dec   <= (others => '0');
            Uni   <= (others => '0');
        elsif(clk_100mhz'event and clk_100mhz='1')then
            state <= next_state;
            Dec   <= next_dec;
            Uni   <= next_uni;
        end if;
    end process;
    
    DECODER_OUTPUT: PROCESS(state, clock, clk_100mhz)
    begin
        next_dec <= Dec;
        next_uni <= Uni;
        case (state) is
            when S0=>--Parado
                LED_Countoff<='1';
                LED_counton<='0';
                
            when S1=>--Contando
                LED_counton<='1';
                LED_Countoff<='0';
                if clock'event and clock = '1' then
                    if Enable = '1' then
                        if Counting_way = '0' then--Cuenta ascendente
                            if Uni = "1111" then 
                            next_Uni <= "0000";
                            if Dec = "0010" then
                            next_dec <= "0000";
                            else
                            next_dec <= dec + 1;
                            end if;
                            else
                            next_Uni <= Uni +  '1';   
                            end if;
                        
                    else if Counting_way = '1' then--Cuenta descendente
                        if Uni = "0000" then 
                        next_Uni <= "1111";
                        if Dec = "0000" then
                        next_Dec <= "0010";
                        else
                        Next_Dec <= Dec - '1';
                        end if;
                        else
                        Next_Uni <= Uni -  '1';      
                        end if;    
                        end if;   
                    end if;  
                end if;
                end if;
    end case;
end PROCESS;  

 NEXT_STATE_DECODE: PROCESS(state, count_on, count_off, clear_n,clock, clk_100mhz)
 begin

 CASE(state) is
 
 when S0=>
 if (count_on = '1') then
 next_state<=S1;
 else
 next_state<=S0;
 end if;

 when S1=>
 if (count_off = '1')then
 next_state<=S0;
 else
 next_state<=S1;
 end if;
  
  end case;
  
 end process;
 
 LEDS: PROCESS(clock)
  begin
  if(clock='1') then
  LED_reloj<='1';
 
  elsif (clock='0') then
  LED_reloj<='0';
  end if;
  end process;
 end  Behavioral;