library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity RELOJ1KHZ is
    Port ( CLK100MHZ : in  STD_LOGIC;
           CLK1KHZ : out  STD_LOGIC);
end RELOJ1KHZ;

architecture Behavioral of RELOJ1KHZ is
signal pulso: STD_LOGIC := '0';
signal contador: integer range 0 to 49999 := 0;

begin
process (CLK100Mhz)
begin
if (CLK100Mhz'event and CLK100Mhz = '1') then 
if (contador = 49999) then
                pulso <= NOT(pulso);
                contador <= 0;
            else
                contador <= contador+1;
            end if;
    end if;
 end process;
 CLK1KHZ<=pulso;
 end Behavioral;
