library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Contador_tb is
--  Port ( );
end Contador_tb;

architecture Behavioral of Contador_tb is

    component contador is
    Port(
        clock: in std_logic;
        Clear_n: in std_logic;
        Enable: in std_logic;
        COunt_on: in std_logic;
        Count_off: in std_logic;
        Counting_way: in std_logic;
        LED_countoff: out std_logic;
        LED_counton: out std_logic;
        clk_100mhz:in std_logic;
        Dec, Uni: inout std_logic_vector(3 DOWNTO 0)
     );
     
     end component;
    
signal clock: std_logic := '0';
signal Clear_n, Enable: std_logic;
signal Counting_way:std_logic:='0';
signal Count_on: std_logic:='0';
signal Count_off: std_logic:='0';
signal Dec, Uni: std_logic_vector(3 DOWNTO 0);
signal LED_Countoff: std_logic:='0';
signal LED_counton: std_logic:='0';
signal clk_100mhz: std_logic:='0';

begin

inst_contador: contador port map(
    clock=>clock,
    clk_100mhz=>clk_100mhz,
    Clear_n=>Clear_n,
    Count_on=>Count_on,
    Count_off=>Count_off,
    Counting_way=>Counting_way,
    Enable=>Enable,
    Dec=>Dec,
    Uni=>Uni,
    LED_Countoff=>LED_Countoff,
    LED_counton=>LED_counton
 );
    
clock <= not clock after 100ns;
clk_100mhz<=not clk_100mhz after 0.1ns;
Counting_way<= not Counting_way after 4000ns;
process
begin 

    Clear_n <= '0' ;
    wait for 425 ns;
    Clear_n<='1';
    wait for 50 ns;
    Enable <= '0';
    wait for 200 ns;
    Enable <= '1';
    wait for 100 ns;
    count_on<='1';
    wait for 200ns;
    count_on<='0';
    wait for 15800 ns;
    Clear_n <= '0' ;
    wait for 1000 ns;
    Clear_n<='1';
    wait for 10000 ns;
    count_on<='1';
    wait for 200ns;
    count_on<='0';
    wait for 1000ns;
    count_off<='1';
    wait for 200ns;
    count_off<='0';
    wait for 1000ns;
    count_on<='1';
    wait for 200ns;
    count_on<='0';
    wait;
end process;
end Behavioral;
