----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2019 17:24:54
-- Design Name: 
-- Module Name: Display_selection_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Display_selection_tb is
--  Port ( );
end Display_selection_tb;

architecture Behavioral of Display_selection_tb is

Component Display_selection is

Port(
    input: in std_logic;
      anodo: out std_logic_vector(3 downto 0));
 end component;
 
signal input: std_logic:='0';
signal anodo: std_logic_vector(3 downto 0);
begin

inst_Display_selection: Display_selection port map(
        input=>input,
        anodo=>anodo);
   
input<=not input after 100ns;
   

end Behavioral;
