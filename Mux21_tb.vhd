----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2019 18:31:59
-- Design Name: 
-- Module Name: Mux21_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux21_tb is
--  Port ( );
end Mux21_tb;

architecture Behavioral of Mux21_tb is

component Mux21 is 
Port(
Decenas  : in  STD_LOGIC_VECTOR (3 downto 0);
           Unidades : in  STD_LOGIC_VECTOR (3 downto 0);
           Selector : in  STD_LOGIC;
           Salidas  : out  STD_LOGIC_VECTOR (3 downto 0));
           end component;

signal selector: std_logic:='0';
signal salidas, unidades, decenas: std_logic_vector (3 downto 0);

begin

inst_mux21: mux21 port map(

 selector=>selector,
 unidades=>unidades,
 decenas=>decenas,
 salidas=>salidas
 );
 
 selector <=not selector after 200ns;
 process
 begin
 unidades<="0101";
 decenas<="0001";
 wait for 5000ns;
 unidades<="1111";
  decenas<="0010";
 wait;
 end process;
end Behavioral;
