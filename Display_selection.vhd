library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity Display_selection is
port( input: in std_logic;
      anodo: out std_logic_vector(3 downto 0));
 end Display_selection;

architecture Behavioral of Display_selection is

begin
process(input)
begin
          case input is
            when '0' => anodo <= "1110";
            when others => anodo <= "1101";
           
         end case;
   
end process;

end Behavioral;
