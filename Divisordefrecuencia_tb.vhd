----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 11:34:04
-- Design Name: 
-- Module Name: Reloj1Khz_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Divisor_tb is
--  Port ( );
end Divisor_tb;

architecture Behavioral of Divisor_tb is

    Component Divisor is
    Port(      CLK100MHZ : in  STD_LOGIC;
               CLK1hz : out  STD_LOGIC
         );
         end component;
     
     signal CLK100MHZ: std_logic:='0';
     signal CLK1hz: STD_LOGIC := '0';
     
begin

inst_Divisor: Divisor port map(
        CLK100MHZ=>CLK100MHZ,
        CLK1Hz => CLK1Hz
        );
    
 CLK100MHZ <= not CLK100MHZ after 0.001ns;
end Behavioral;
