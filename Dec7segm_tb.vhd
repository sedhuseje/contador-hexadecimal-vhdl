----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 11:34:04
-- Design Name: 
-- Module Name: Reloj1Khz_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dec7segm_tb is
--  Port ( );
end Dec7segm_tb;

architecture Behavioral of Dec7segm_tb is

    Component Dec7segm is
    Port(     Binario : in  STD_LOGIC_VECTOR (3 downto 0);
              led: out STD_LOGIC_VECTOR(6 downto 0) 
         );
         end component;
     
     signal binario: std_logic_vector(3 downto 0);
     signal Led: STD_LOGIC_vector(6 downto 0);
     
begin

inst_Dec7segm: Dec7segm port map(
        Binario=>Binario,
        led =>led
        );
 process
 begin
 Binario <= "0001";
 wait for 450ns;
 Binario <= "0011";
wait;
end process;
end Behavioral;
